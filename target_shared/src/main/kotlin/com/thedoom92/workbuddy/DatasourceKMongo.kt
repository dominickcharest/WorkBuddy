package com.thedoom92.workbuddy

import org.litote.kmongo.coroutine.coroutine
import org.litote.kmongo.reactivestreams.KMongo

class DatasourceKMongo (private val connectionString: String) : Datasource {
    val connection = KMongo.createClient(connectionString).coroutine
}