package com.thedoom92.workbuddy.util

import java.lang.NumberFormatException
import kotlin.jvm.Throws


// Parse the string to Int, if it succeeds and succeeds validation (lambda) it's returned
// Otherwise it tries to return default value, or null, or crash.
@Throws(IllegalArgumentException::class)
fun getIntParam(
    raw: String?,
    default: Int? = null,
    nullable: Boolean = true,
    messageIfInvalid: String = "Invalid argument",
    moreValidation: (Int) -> Boolean = { true }
) : Int? {

    try {
        if (!raw.isNullOrEmpty()) {
            val result =  raw.toInt()
            if(moreValidation(result)) return result
        }
    } catch (_: NumberFormatException) {} // Safely ignore, handled under

    if (default != null) return default
    if(nullable) return null

    throw IllegalArgumentException(messageIfInvalid)

}