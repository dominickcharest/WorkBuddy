package com.thedoom92.workbuddy.util

import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*

class Time {

    companion object {
        fun today(): String {
            val current = LocalDateTime.now()
            val formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd")
            return current.format(formatter)
        }

        fun curWeek(): Long {
            return Calendar.getInstance().get(Calendar.WEEK_OF_YEAR).toLong()
        }
    }
}