package com.thedoom92.workbuddy.util

class DataNotFoundException(message: String? = null, cause: Throwable? = null) : Exception(message, cause)