package com.thedoom92.workbuddy.feature_daytracker.entity

import kotlinx.serialization.Serializable

@Serializable
data class Day(
    val date: String,
    val shifts: MutableList<Shift>,
    val locations: MutableList<String>
)
