package com.thedoom92.workbuddy.feature_daytracker.data

import com.thedoom92.library.time.Moment
import com.thedoom92.workbuddy.feature_daytracker.DayTrackerDAO
import com.thedoom92.workbuddy.feature_daytracker.entity.Day
import com.thedoom92.workbuddy.feature_daytracker.entity.Shift
import com.thedoom92.workbuddy.util.Time
import org.litote.kmongo.MongoOperator

class DayTrackerDaoMemory : DayTrackerDAO {

    override suspend fun updateDay(day: Day) {
        database[day.date] = day
    }

    override suspend fun getDay(date: String): Day? {
        return database[date]
    }

    override suspend fun getWeek(week: Int): List<Day> {
        return getWeeks(week..week)
    }

    override suspend fun getWeeks(weeks: IntRange): List<Day> {
        TODO("Not yet implemented")
    }

    override suspend fun initializeCurrentDay(): Day {
        getDay(Time.today())?.let { return it }

        val day = Day(
            Time.today(),
            mutableListOf(),
            mutableListOf()
        )

        updateDay(day)
        return day
    }

    private val database: MutableMap<String, Day> = mutableMapOf(
        "2021-12-13" to Day(date = "2021-12-13", shifts = mutableListOf(), locations = mutableListOf()),
        "2021-12-14" to Day(
            date = "2021-12-14",
            shifts = mutableListOf(
                Shift(Moment.fromNow(), Moment.fromNow(), "Calimero", "Peinture, Portes"),
            ),
            locations = mutableListOf("Home", "Cali", "Home")
        ),
        "2021-12-15" to Day(
            date = "2021-12-15",
            shifts = mutableListOf(
                Shift(Moment.fromNow(), Moment.fromNow(), "Mimi-Coco", "Réparer une tôle coupante"),
                Shift(Moment.fromNow(), Moment.fromNow(), "Maison des Enfants", "Réparations et travaux divers intérieur"),
            ),
            locations = mutableListOf("Home", "Home Depot", "MC", "MdE", "Home")
        ),
        "2021-12-16" to Day(date = "2021-12-16", shifts = mutableListOf(), locations = mutableListOf()),
        "2021-12-17" to Day(
            date = "2021-12-17",
            shifts = mutableListOf(
                Shift(Moment.fromNow(), Moment.fromNow(), "Nouvelle Lune", "Clôture à neige, Tapis, Hottes, aimant du gros meuble"),
                Shift(Moment.fromNow(), Moment.fromNow(), "Maison des Enfants", "Poubelle extérieur, Évier, Programmer une lumière"),
                Shift(Moment.fromNow(), Moment.fromNow(), "Caliméro", "Bloc de béton, poignés, plywood"),
            ),
            locations = mutableListOf("Home", "BC", "NL", "Canac", "Metro", "NL", "MdE", "Cali", "Home")
        ),
        "2021-12-18" to Day(
            date = "2021-12-18",
            shifts = mutableListOf(), locations = mutableListOf()
        ),
        "2021-12-19" to Day(
            date = "2021-12-19",
            shifts = mutableListOf(
                Shift(Moment.fromNow(), Moment.fromNow(), "Maison des Enfants", "Plâtre"),
            ),
            locations = mutableListOf("Home", "MdE", "Home")
        ),
        "2021-12-20" to Day(
            date = "2021-12-20",
            shifts = mutableListOf(
                Shift(Moment.fromNow(), Moment.fromNow(), "Caliméro", "Meuble et jouets pour Noël"),
            ),
            locations = mutableListOf("Home", "Cali", "Home")
        ),
        "2021-12-21" to Day(
            date = "2021-12-21",
            shifts = mutableListOf(
                Shift(Moment.fromNow().copy(hour = Moment.fromNow().hour-4), Moment.fromNow(), "Mimi-Coco", "Shopping"),
                Shift(Moment.fromNow(), Moment.fromNow(), "Maison Enchantée", "Installer un tapis anti-dérapant"),
                Shift(Moment.fromNow(), Moment.fromNow(), "Maison des Enfants", "Réparer un vélo"),
                Shift(Moment.fromNow(), Moment.fromNow(), "Arche de Noé", "Livraison Plywood"),
                Shift(Moment.fromNow(), Moment.fromNow(), "Bureaux", "Replacer le tapis"),
                Shift(Moment.fromNow(), null, "Caliméro", "Installer un tapis"),
            ),
            locations = mutableListOf(
                "Home",
                "Home Depot",
                "Canadian Tire St-Joseph",
                "Walmart St-Joseph",
                "ME",
                "MdE",
                "AdN",
                "BC",
                "Cali",
                "Home"
            )
        ),
        "2021-12-22" to Day(
            date = "2021-12-22",
            shifts = mutableListOf(
                Shift(Moment.fromNow(), Moment.fromNow(), "Mimi-Coco", "Hauteur de la cloture"),
                Shift(Moment.fromNow(), Moment.fromNow(), "Mimi-Coco", "Nettoyage HVAC"),
            ),
            locations = mutableListOf("Home", "MC", "Home")
        ),
        "2021-12-23" to Day(date = "2021-12-23", shifts = mutableListOf(), locations = mutableListOf()),
        "2021-12-24" to Day(date = "2021-12-24", shifts = mutableListOf(), locations = mutableListOf()),
        "2021-12-25" to Day(date = "2021-12-25", shifts = mutableListOf(), locations = mutableListOf()),
        "2021-12-26" to Day(date = "2021-12-26", shifts = mutableListOf(), locations = mutableListOf()),
    )
}