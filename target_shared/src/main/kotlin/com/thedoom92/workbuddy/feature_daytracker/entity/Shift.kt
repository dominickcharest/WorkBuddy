package com.thedoom92.workbuddy.feature_daytracker.entity

import com.thedoom92.library.time.Moment
import kotlinx.serialization.Serializable

@Serializable
data class Shift(
    var start_time: Moment,
    var end_time: Moment? = null,
    var customer: String? = null,
    var task: String? = null,
    var notes: String? = null
)
