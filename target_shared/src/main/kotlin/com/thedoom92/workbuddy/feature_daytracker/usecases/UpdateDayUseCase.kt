package com.thedoom92.workbuddy.feature_daytracker.usecases

import com.thedoom92.workbuddy.feature_daytracker.DayTrackerRepository
import com.thedoom92.workbuddy.feature_daytracker.entity.Day

class UpdateDayUseCase(
    private val repository: DayTrackerRepository
) {

    suspend operator fun invoke(day: Day) {
        repository.updateDay(day)
    }
}