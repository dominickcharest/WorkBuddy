package com.thedoom92.workbuddy.feature_daytracker.data

import com.thedoom92.workbuddy.DatasourceKMongo
import com.thedoom92.workbuddy.feature_daytracker.DayTrackerDAO
import com.thedoom92.workbuddy.feature_daytracker.entity.Day

class DayTrackerDaoKMongo(
    private val datasource: DatasourceKMongo
) : DayTrackerDAO {

    private val collection by lazy {
        datasource
            .connection
            .getDatabase("WorkBuddy")
            .getCollection<Day>() }

    // TODO: Moment
    override suspend fun getDay(date: String): Day {

        //val t = date.toEpochMilliseconds()/1000
        //println(t)
        //return collection.findOne(WorkDay::date eq t)
        //    ?: throw DataNotFoundException("Couldn't find date")
        TODO("Not yet implemented")
    }

    override suspend fun updateDay(day: Day) {
        TODO("Not yet implemented")
    }

    override suspend fun getWeek(week: Int): List<Day> {
        TODO("Not yet implemented")
    }

    override suspend fun getWeeks(weeks: IntRange): List<Day> {
        TODO("Not yet implemented")
    }

    override suspend fun initializeCurrentDay(): Day {
        TODO("Not yet implemented")
    }

}