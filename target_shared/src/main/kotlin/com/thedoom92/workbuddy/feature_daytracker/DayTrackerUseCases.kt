package com.thedoom92.workbuddy.feature_daytracker

import com.thedoom92.workbuddy.feature_daytracker.usecases.GetWeekUseCase
import com.thedoom92.workbuddy.feature_daytracker.usecases.GetDayUseCase
import com.thedoom92.workbuddy.feature_daytracker.usecases.UpdateDayUseCase

data class DayTrackerUseCases (
    val updateDay: UpdateDayUseCase,
    val getDay: GetDayUseCase,
    val getWeek: GetWeekUseCase,
)