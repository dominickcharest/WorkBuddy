package com.thedoom92.workbuddy.feature_daytracker

import com.thedoom92.workbuddy.feature_daytracker.entity.Day
import com.thedoom92.workbuddy.util.Time

interface DayTrackerRepository {

    suspend fun updateDay(day: Day)
    suspend fun getDay(date: String): Day?
    suspend fun getWeek(week: Int): List<Day>
    suspend fun getWeeks(weeks: IntRange): List<Day>
    suspend fun initializeCurrentDay(): Day
}