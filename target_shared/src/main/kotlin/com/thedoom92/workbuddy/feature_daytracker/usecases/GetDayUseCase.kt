package com.thedoom92.workbuddy.feature_daytracker.usecases

import com.thedoom92.workbuddy.feature_daytracker.DayTrackerRepository
import com.thedoom92.workbuddy.feature_daytracker.entity.Day
import com.thedoom92.workbuddy.util.Resource
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class GetDayUseCase(private val repository: DayTrackerRepository) {


    operator fun invoke(date: String): Flow<Resource<Day>> = flow {
        //TODO: Verify date
        try {
            emit(Resource.Loading())
            val day = repository.getDay(date) ?: repository.initializeCurrentDay()
            emit(Resource.Success(day))
        } catch (e: Exception) {
            emit(Resource.Error(e.localizedMessage ?: "An Exception occured"))
        }
    }
}