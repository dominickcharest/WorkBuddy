val ktSerializationVer: String by project
val kmongoVer: String by project

plugins {
    kotlin("jvm")
    kotlin("plugin.serialization") version "1.6.10"
}

repositories {
    mavenCentral()
}

dependencies {
    implementation(project(":library"))
    implementation(kotlin("stdlib"))

    implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:$ktSerializationVer")

    implementation("org.litote.kmongo:kmongo-coroutine-serialization:$kmongoVer")
}