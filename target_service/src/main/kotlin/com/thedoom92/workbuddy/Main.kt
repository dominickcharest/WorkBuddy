package com.thedoom92.workbuddy

import com.thedoom92.workbuddy.DIMain.registerRoutes
import io.ktor.application.*
import io.ktor.features.*
import io.ktor.request.*
import io.ktor.serialization.*
import io.ktor.server.engine.*
import io.ktor.server.netty.*
import org.slf4j.event.Level

/*
This module is the presentation layer for the REST Api, the
domain and data layers are in the target_shared module. Refer
to the DI* (Dependency Injection) objects to see how everything
is glued together
 */
fun main() {
    embeddedServer(Netty, port = 8000, host = "127.0.0.1") {
        install(ContentNegotiation) {
            json()
        }

        install(CallLogging) {
            level = Level.INFO
            filter { call -> call.request.path().startsWith("/") }
        }

        // See DIMain
        registerRoutes()

        println("Starting Server on 127.0.0.1:8000")
    }.start(wait = true)
}