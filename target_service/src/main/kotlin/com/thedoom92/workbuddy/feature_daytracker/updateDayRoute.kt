package com.thedoom92.workbuddy.feature_daytracker

import com.thedoom92.workbuddy.feature_daytracker.entity.Shift
import io.ktor.application.*
import io.ktor.request.*
import io.ktor.routing.*

fun Route.updateDay(useCases: DayTrackerUseCases) {
    post {
        val shift = call.receive<Shift>()
        // validate data


        useCases.updateDay()
    }
}