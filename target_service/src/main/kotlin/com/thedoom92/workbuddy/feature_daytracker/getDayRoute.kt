package com.thedoom92.workbuddy.feature_daytracker

import com.thedoom92.workbuddy.feature_daytracker.entity.Day
import io.ktor.application.*
import io.ktor.http.*
import io.ktor.response.*
import io.ktor.routing.*

fun Route.getDay(useCases: DayTrackerUseCases) {
    get("/{year}/{month}/{day}") {
        val year = call.parameters["year"];
        val month = call.parameters["month"];
        val day = call.parameters["day"];

        val workDay = useCases.getDay("$year-$month-$day")
        /*if(workDay.success) {
            call.respond(workDay.data as Day)
        } else {
            call.respondText(workDay.data as String, status = HttpStatusCode.NotFound)
        }*/
    }
}