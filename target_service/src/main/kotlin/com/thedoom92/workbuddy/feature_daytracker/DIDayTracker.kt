package com.thedoom92.workbuddy.feature_daytracker

import com.thedoom92.workbuddy.feature_daytracker.data.DayTrackerDaoMemory
import com.thedoom92.workbuddy.feature_daytracker.usecases.UpdateDayUseCase
import com.thedoom92.workbuddy.feature_daytracker.usecases.GetWeekUseCase
import com.thedoom92.workbuddy.feature_daytracker.usecases.GetDayUseCase
import io.ktor.routing.*

// Dependency Injection for DayTracker, all the business logic comes from target_shared
object DIDayTracker{

    private val dao = DayTrackerDaoMemory()

    private val repository = DayTrackerServiceRepository(dao)

    private val actions = DayTrackerUseCases(
        UpdateDayUseCase(repository),
        GetDayUseCase(repository),
        GetWeekUseCase(repository)
    )

    fun Route.registerDayTrackerRoutes() {
        route("/daytracker") {
            updateDay(actions)
            getDay(actions)
            getWeek(actions)
        }
    }
}