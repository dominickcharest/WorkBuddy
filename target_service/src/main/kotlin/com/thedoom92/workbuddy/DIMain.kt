package com.thedoom92.workbuddy

import com.thedoom92.workbuddy.feature_daytracker.DIDayTracker.registerDayTrackerRoutes
import io.ktor.application.*
import io.ktor.routing.*

object DIMain {

    //val mongoDatabase = DatasourceKMongo(
    //    "mongodb+srv://thedoom92:iliketrains@cluster0.xryob.mongodb.net/TimeBud" +
    //                "?retryWrites=true&w=majority")

    fun Application.registerRoutes(){
        routing {
            route("/api/v1") {

                // See DIDayTracker
                registerDayTrackerRoutes()
            }
        }
    }
}

