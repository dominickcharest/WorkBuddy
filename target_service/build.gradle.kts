plugins {
    application
    kotlin("jvm")
    kotlin("plugin.serialization") version "1.6.10"
}

repositories {
    mavenCentral()
}

application {
    mainClass.set("com.thedoom92.workbuddy.MainKt")
}

dependencies {
    implementation(project(":library"))
    implementation(project(":target_shared"))

    // Core
    val ktKotlinVer: String by project
    implementation(kotlin("stdlib"))
    testImplementation("org.jetbrains.kotlin:kotlin-test-junit:$ktKotlinVer")
    testImplementation("org.jetbrains.kotlin:kotlin-test:$ktKotlinVer")

    // JetBrains KTor
    val ktKTorVer: String by project
    val logbackVer: String by project
    implementation("io.ktor:ktor-server-core:$ktKTorVer")
    implementation("io.ktor:ktor-server-netty:$ktKTorVer")
    implementation("io.ktor:ktor-serialization:$ktKTorVer")
    implementation("io.ktor:ktor-server-host-common:$ktKTorVer")
    testImplementation("io.ktor:ktor-server-tests:$ktKTorVer")
    implementation("ch.qos.logback:logback-classic:$logbackVer")
}