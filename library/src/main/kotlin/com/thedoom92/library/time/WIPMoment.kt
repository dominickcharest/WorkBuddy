package com.thedoom92.library.time

import kotlinx.serialization.Serializable

@Serializable
class WIPMoment private constructor(
    val timestamp: Long,
    val year: Int,
    val month: Int,
    val day: Int,
    val hour: Int,
    val minute: Int,
    val second: Int,
    val isLeap: Boolean,
) {
    init {
        // Computing other values
    }


    fun copy(
        timestamp: Long = this.timestamp,
        year: Int = this.year,
        month: Int = this.month,
        day: Int = this.day,
        hour: Int = this.hour,
        minute: Int = this.minute,
        second: Int = this.second,
        isLeap: Boolean = this.isLeap,
    ): WIPMoment {
        return WIPMoment(timestamp, year, month, day, hour, minute, second, isLeap)
    }

    /***************
    Operators
     ***************/

    override fun toString(): String {
        return "[$year-$month-$day $hour:$minute:$second]"
    }

    fun format(format: String): String {
        var result = ""
        var accumulator = ""
        var escaping = false
        var triggerParser = false

        println("Format")

        format.forEachIndexed { i, it ->
            println("$i -> $it")

            if ((accumulator.isNotBlank() && it != accumulator[0]))
                triggerParser = true

            if(i == format.length-1) {
                triggerParser = true
                accumulator += it
            }

            if(triggerParser){
                result += when (accumulator) {
                    "MM" -> "LALALA"
                    "MM" -> "LALALA"
                    "MM" -> "LALALA"
                    "MM" -> "LALALA"
                    "MM" -> "LALALA"
                    "MM" -> "LALALA"
                    "MM" -> "LALALA"
                    "MM" -> "LALALA"
                    "MM" -> "LALALA"
                    "MM" -> "LALALA"
                    "MM" -> "LALALA"
                    "MM" -> "LALALA"
                    "MM" -> "LALALA"
                    "MM" -> "LALALA"
                    "MM" -> "LALALA"
                    "MM" -> "LALALA"
                    "MM" -> "LALALA"
                    "MM" -> "LALALA"
                    "MM" -> "LALALA"
                    "MM" -> "LALALA"
                    "MM" -> "LALALA"
                    "MM" -> "LALALA"
                    "MM" -> "LALALA"
                    "MM" -> "LALALA"
                    "MM" -> "LALALA"
                    else -> accumulator
                }
                accumulator = ""
                triggerParser = false
            }

            if (escaping) {
                result += when (it) {
                    'n' -> "\n"
                    't' -> "\t"
                    else -> it
                }
                escaping = false
                return@forEachIndexed
            }

            if (it == '\\') {
                escaping = true
                return@forEachIndexed
            }

            accumulator += it
        }

        return result
    }

    companion object {
        val secsInAYear = 31536000
        val secsInADay = 86400

        /*********************
        Factory Methods
         *********************/
        fun fromNow(): WIPMoment {
            return fromTimestamp(System.currentTimeMillis() / 1000)
        }

        fun fromDate(
            year: Int = 1970, month: Int = 1, day: Int = 1,
            hour: Int = 0, minute: Int = 0, second: Int = 0
        ): WIPMoment {

            // User Input Validation
            if (year < 1970) throw NotImplementedError("Negative timestamps aren't implemented yet")
            if (year !in 0..9999) throw InvalidDateException("Invalid Year: $year")
            if (month !in 1..12) throw InvalidDateException("Invalid Month: $month")
            if (day !in 1..31) throw InvalidDateException("Invalid Day: $day")
            if (hour !in 0..24) throw InvalidDateException("Invalid Hour: $hour")
            if (minute !in 0..60) throw InvalidDateException("Invalid Minute: $minute")
            if (second !in 0..60) throw InvalidDateException("Invalid Second: $second")

            // Date structure validation
            if (day > daysInThisMonth(year, month)) throw InvalidDateException("Invalid Day: $day")

            // Getting and returning the Moment
            return dateToTimestamp(year, month, day, hour, minute, second)
        }

        fun fromTimestamp(timestamp: Long): WIPMoment {
            if (timestamp < 0) throw NotImplementedError("Negative timestamps aren't implemented yet")

            return timestampToDate(timestamp)
        }

        fun fromISOString(): WIPMoment {
            TODO("Not implemented yet")
        }

        /***************
        Utilities
         ***************/

        private fun isLeapYear(year: Int): Boolean {
            if (year % 4 != 0) return false
            if (year % 100 == 0 && year % 400 != 0) return false
            return true
        }

        private fun daysInThisMonth(year: Int, month: Int): Int {
            return when (month) {
                1, 3, 5, 7, 8, 10, 12 -> 31
                4, 6, 9, 11 -> 30
                2 -> if (isLeapYear(year)) 29 else 28

                else -> throw InvalidDateException("Invalid Month, this really shouldn't happen")
            }
        }

        private fun monthAndDayFromDays(days: Long, leap: Boolean): Pair<Long, Long> {

            // Remove leap day if applicable, shortens the function and complexity a lot.
            val d = if (leap && days > 59) days - 1 else days

            if (d > 365) throw IllegalArgumentException("Value is too big")

            val ret = when (d) {
                in 1..31 -> listOf(1L, d)
                in 32..59 -> listOf(2L, d % 31)
                in 60..90 -> listOf(3L, d % 59)
                in 91..120 -> listOf(4L, d % 90)
                in 121..151 -> listOf(5L, d % 120)
                in 152..181 -> listOf(6L, d % 151)
                in 182..212 -> listOf(7L, d % 181)
                in 213..243 -> listOf(8L, d % 212)
                in 244..273 -> listOf(9L, d % 243)
                in 274..304 -> listOf(10L, d % 273)
                in 305..334 -> listOf(11L, d % 304)
                in 335..365 -> listOf(12L, d % 334)
                else -> throw WTFException("$days")
            }

            // Adding 29th of February back
            val day = if (leap && days == 60L) ret[1] + 1 else ret[1]

            return Pair(ret[0], day)
        }

        /************************
        Processing Methods
         ************************/

        private fun dateToTimestamp(year: Int, month: Int, day: Int, hour: Int, minute: Int, second: Int): WIPMoment {
            // Years
            var ret: Long = ((year - 1970) * secsInAYear).toLong()

            // Months
            val isLeap = isLeapYear(year)
            val monthList = if (!isLeap) {
                listOf(0, 0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334)
            } else {
                listOf(0, 0, 31, 60, 91, 121, 152, 182, 213, 244, 274, 305, 335)
            }
            ret += monthList[month] * secsInADay

            // Days
            // 1-based to 0-based
            ret += (day - 1) * secsInADay
            // Adjust for leap days
            if (year > 1972) ret += (year - 1968) / 4 * secsInADay
            if (isLeap && (monthList[month] + day) < 59) ret -= secsInADay

            // Time
            ret += hour * 3600
            ret += minute * 60
            ret += second

            return WIPMoment(ret, year, month, day, hour, minute, second, isLeap)
        }

        private fun timestampToDate(timestamp: Long): WIPMoment {
            // To allow easy timestamp calculation, move the base timestamp to 1969-1-1
            val tstamp = timestamp + 31536000

            // Number of leaps since Unix Epoch
            var days: Long = tstamp / 86400
            val leaps = days / (365 * 4 + 1)
            days -= leaps

            // Calculating the year
            val year = days / 365
            val isLeap = isLeapYear((year + 1969).toInt())

            // Getting the day and month
            days %= 365
            days++ // 0 based to 1 based
            val (month, day) = monthAndDayFromDays(days, isLeap)

            // Parsing the time
            var time = tstamp % 86400
            val seconds = time % 60
            time -= seconds
            val minutes = (time % 3600) / 60
            time -= minutes
            val hours = time / 3600

            return WIPMoment(
                timestamp,
                (year + 1969).toInt(), month.toInt(), day.toInt(),
                hours.toInt(), minutes.toInt(), seconds.toInt(), isLeap
            )
        }

        /****************
        Exceptions
         ****************/

        class InvalidDateException(message: String? = null, cause: Throwable? = null) : Exception(message, cause)
        class WTFException(override val message: String? = null) : Exception("Shit just hit the fan badly: $message")
    }
}
