package com.thedoom92.library.time

import kotlinx.serialization.Contextual
import kotlinx.serialization.Serializable
import java.time.Duration
import java.time.Instant
import java.time.ZoneId
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter

@Serializable
class Moment private constructor(
    val date: @Contextual ZonedDateTime
) {
    val year: Int
        get() = date.year

    val month: Int
        get() = date.monthValue

    val day: Int
        get() = date.dayOfMonth

    val hour: Int
        get() = date.hour

    val minute: Int
        get() = date.minute

    val second: Int
        get() = date.second

    val millis: Int
        get() = date.nano

    val timestamp: Long
        get() = date.toEpochSecond()

    fun copy(date: ZonedDateTime = this.date): Moment {
        return Moment(date)
    }

    fun copy (
        year: Int = this.year,
        month: Int = this.month,
        day: Int = this.day,
        hour: Int = this.hour,
        minute: Int = this.minute,
        second: Int = this.second,
        millis: Int = this.millis,
    ): Moment {
        return fromDate(year, month, day, hour, minute, second, millis)
    }

    override fun toString(): String {
        return date.toString()
    }

    fun format(format: String): String {
        return date.format(DateTimeFormatter.ofPattern(format))
    }

    companion object {
        fun fromTimestamp(timestamp: Long): Moment {
            val inst = Instant.ofEpochSecond(timestamp)
            val date = ZonedDateTime.ofInstant(inst, ZoneId.systemDefault())
            return Moment(date)
        }

        fun fromDate(
            year: Int = 2020,
            month: Int = 1,
            day: Int = 1,
            hour: Int = 0,
            minute: Int = 0,
            second: Int = 0,
            millis: Int = 0,
        ): Moment {
            val date = ZonedDateTime.of(year, month, day, hour, minute, second, millis, ZoneId.systemDefault())
            return Moment(date)
        }

        fun fromNow(): Moment {
            val date = ZonedDateTime.now(ZoneId.systemDefault())
            return Moment(date)
        }
    }

    class Interval (start: Moment, end: Moment) {
        private val duration: Duration
        init {
            duration = Duration.between(start.date, end.date)
        }

        fun print() : String {
            val hours = duration.toHours()
            val minutes = duration.toMinutes() - (hours*60)
            return "${hours}h${minutes.toString().padStart(2,'0')}"
        }
    }
}

