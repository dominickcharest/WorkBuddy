package com.thedoom92.library.time

import com.thedoom92.library.time.WIPMoment.Companion.fromDate
import com.thedoom92.library.time.WIPMoment.Companion.fromTimestamp
import kotlin.test.Test
import kotlin.test.assertEquals

class WIPMomentTests {

    fun <T> test(expected: T, result: T, message: String) {
        println(message)
        assertEquals(expected, result)
    }


    @Test
    fun testFromTimestamp() {
        val testName = "fromTimestamp()"

        test("[2021-12-3 2:52:25]",
            fromTimestamp(1638499945).toString(),
            "$testName [1] Random Date")

        test("[1970-1-1 0:0:0]",
            fromTimestamp(0).toString(),
            "$testName [2] Unix Epoch")

        test("[1972-2-28 23:59:59]",
            fromTimestamp(68169599).toString(),
            "$testName [3] Just before leap")

        test("[1972-6-23 9:12:43]",
            fromTimestamp(78138763).toString(),
            "$testName [4] Random day in leap year")

        test("[1972-2-29 0:0:0]",
            fromTimestamp(68169600).toString(),
            "$testName [5] Leap day")

        test("[1972-3-1 0:0:0]",
            fromTimestamp(68256000).toString(),
            "$testName [6] Day after leap")

        test("[1992-1-1 0:0:0]",
            fromTimestamp(694224000).toString(),
            "$testName [7] Random leap")

        test("[2021-1-1 0:0:0]",
            fromTimestamp(1609459200).toString(),
            "$testName [8] Non-leap start of year")

        test("[2021-12-31 23:59:59]",
            fromTimestamp(1640995199).toString(),
            "$testName [9] Non-leap last second of year")

        test("[2022-1-1 0:0:0]",
            fromTimestamp(1640995200).toString(),
            "$testName [10] Leap start of year")

        test("[2022-12-31 23:59:59]",
            fromTimestamp(1672531199).toString(),
            "$testName [11] Leap last second of year")
    }

    @Test
    fun testFrom() {
        val testName = "fromDate()"

        test(1638499945,
            fromDate(2021, 12, 3, 2, 52, 25).timestamp,
            "$testName [1] Random Date")

        test(0,
            fromDate(1970, 1, 1, 0, 0, 0).timestamp,
            "$testName [2] Unix Epoch")

        test(68169599,
            fromDate(1972, 2, 28, 23, 59, 59).timestamp,
            "$testName [3] Just before leap")

        test(78138763,
            fromDate(1972, 6, 23, 9, 12, 43).timestamp,
            "$testName [4] Random day in leap year")

        test(68169600,
            fromDate(1972, 2, 29, 0,0, 0).timestamp,
            "$testName [5] Leap day")

        test(68256000,
            fromDate(1972, 3, 1, 0, 0, 0).timestamp,
            "$testName [6] Day after leap")

        test(694224000,
            fromDate(1992, 1, 1, 0, 0, 0).timestamp,
            "$testName [7] Random leap")

        test(1609459200,
            fromDate(2021, 1, 1, 0, 0, 0).timestamp,
            "$testName [8] Non-leap start of year")

        test(1640995199,
            fromDate(2021, 12, 31, 23, 59, 59).timestamp,
            "$testName [9] Non-leap last second of year")

        test(1640995200,
            fromDate(2022, 1, 1, 0, 0, 0).timestamp,
            "$testName [10] Leap start of year")

        test(1672531199,
            fromDate(2022, 12, 31, 23, 59, 59).timestamp,
            "$testName [11] Leap last second of year")
    }
}