package com.thedoom92.workbuddy

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.navigation.compose.rememberNavController
import com.thedoom92.workbuddy.navigation.Route
import com.thedoom92.workbuddy.theme.WorkBuddyTheme

class MainActivity : ComponentActivity() {
	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)

		setContent {
			WorkBuddyTheme {
				val navController = rememberNavController()
				Route.CreateNavGraph(navController)
			}
		}
	}
}