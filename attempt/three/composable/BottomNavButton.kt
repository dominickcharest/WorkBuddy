package com.thedoom92.workbuddy.composable

import android.content.res.Configuration.UI_MODE_NIGHT_YES
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview

@Composable
fun BottomNavButton(
    modifier: Modifier = Modifier,
    onClickAction: () -> Unit = {}
) {
    Button(
        onClick = onClickAction,
        modifier = modifier
    ){
        Text("Dick")
    }
}