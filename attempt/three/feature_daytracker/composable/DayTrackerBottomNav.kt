package com.thedoom92.workbuddy.feature_daytracker.composable

import android.content.res.Configuration.UI_MODE_NIGHT_YES
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import com.thedoom92.workbuddy.composable.BottomNavButton
import com.thedoom92.workbuddy.feature_daytracker.navigation.DayTrackerRoutes
import com.thedoom92.workbuddy.navigation.RouteDefinition
import com.thedoom92.workbuddy.theme.WorkBuddyTheme


@Preview(
    showBackground = true,
    uiMode = UI_MODE_NIGHT_YES
)
@Composable
fun preview(){
    WorkBuddyTheme { DayTrackerBottomNav(currentScreen = DayTrackerRoutes.viewDay) }
}


@Composable
fun DayTrackerBottomNav(
    dayButtonAction: () -> Unit = {},
    weekButtonAction: () -> Unit = {},
    archiveButtonAction: () -> Unit = {},
    currentScreen: RouteDefinition
){
    Row(
        modifier = Modifier
            .fillMaxWidth()

    ) {
        BottomNavButton(
            modifier = Modifier.weight(1f),
            onClickAction = dayButtonAction)

        BottomNavButton(
            modifier = Modifier.weight(1f),
            onClickAction = weekButtonAction)

        BottomNavButton(
            modifier = Modifier.weight(1f),
            onClickAction = archiveButtonAction)
    }
}
