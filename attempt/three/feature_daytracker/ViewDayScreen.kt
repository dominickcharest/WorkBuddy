package com.thedoom92.workbuddy.feature_daytracker

import android.os.Bundle
import androidx.compose.material.Scaffold
import androidx.compose.runtime.Composable
import com.thedoom92.workbuddy.feature_daytracker.composable.DayTrackerBottomNav
import com.thedoom92.workbuddy.feature_daytracker.navigation.DayTrackerRoutes

@Composable
fun ViewDayScreen(arguments: Bundle?) {
    Scaffold(
        bottomBar = { DayTrackerBottomNav(currentScreen = DayTrackerRoutes.viewDay) }
    ) {

    }
}