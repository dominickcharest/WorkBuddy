package com.thedoom92.workbuddy.feature_daytracker.navigation

import android.os.Bundle
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.AccountCircle
import androidx.navigation.NavController
import androidx.navigation.NavType
import androidx.navigation.navArgument
import com.thedoom92.workbuddy.feature_daytracker.ViewDayScreen
import com.thedoom92.workbuddy.navigation.RouteDefinition

object RouteViewDay : RouteDefinition{
    override val name = "View Day"
    override val icon = Icons.Rounded.AccountCircle
    override val route = "daytracker/dayview/{date}"
    override val arguments = listOf(
        navArgument(name = "date") {
            type = NavType.StringType
        }
    )
    override val content = { args: Bundle? -> ViewDayScreen(args) }

    // TODO: Date Validation
    operator fun invoke(navController: NavController, date: String) {
        navController.navigate(route.replace("{date}", date))
    }
}