package com.thedoom92.workbuddy.feature_daytracker.navigation

import android.os.Bundle
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.AccountCircle
import androidx.navigation.NamedNavArgument
import androidx.navigation.NavController
import com.thedoom92.workbuddy.feature_daytracker.ViewWeekScreen
import com.thedoom92.workbuddy.navigation.RouteDefinition

object RouteViewCurrentWeek : RouteDefinition{
    override val name = "View Current Week"
    override val icon = Icons.Rounded.AccountCircle
    override val route = "daytracker/weekview"
    override val arguments: List<NamedNavArgument> = emptyList()
    override val content = { _: Bundle? -> ViewWeekScreen(null) }

    // TODO: Date Validation
    operator fun invoke(navController: NavController) {
        navController.navigate(route)
    }
}