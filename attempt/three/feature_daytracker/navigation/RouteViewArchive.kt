package com.thedoom92.workbuddy.feature_daytracker.navigation

import android.os.Bundle
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.AccountCircle
import androidx.compose.runtime.Composable
import androidx.navigation.NamedNavArgument
import androidx.navigation.NavController
import com.thedoom92.workbuddy.feature_daytracker.ViewArchiveScreen
import com.thedoom92.workbuddy.navigation.RouteDefinition

object RouteViewArchive : RouteDefinition{
    override val name = "View Archive"
    override val icon = Icons.Rounded.AccountCircle
    override val route = "daytracker/archive"
    override val arguments: List<NamedNavArgument> = emptyList()

    override val content = @Composable { args: Bundle? -> ViewArchiveScreen(args) }

    // TODO: Date Validation
    operator fun invoke(navController: NavController) {
        navController.navigate(route)
    }
}