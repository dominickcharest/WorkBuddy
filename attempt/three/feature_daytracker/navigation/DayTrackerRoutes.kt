package com.thedoom92.workbuddy.feature_daytracker.navigation

import com.thedoom92.workbuddy.navigation.ModuleRoutes

object DayTrackerRoutes : ModuleRoutes {
    val viewCurrentDay = RouteViewCurrentDay
    val viewDay = RouteViewDay
    val viewCurrentWeek = RouteViewCurrentWeek
    val viewWeek = RouteViewWeek
    val viewArchive = RouteViewArchive

    override val asList = listOf(
        viewCurrentDay,
        viewDay,
        viewCurrentWeek,
        viewWeek,
        viewArchive
    )
}