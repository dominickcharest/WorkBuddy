package com.thedoom92.workbuddy.feature_daytracker.navigation

import android.os.Bundle
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.AccountCircle
import androidx.navigation.NavController
import androidx.navigation.NavType
import androidx.navigation.navArgument
import com.thedoom92.workbuddy.feature_daytracker.ViewWeekScreen
import com.thedoom92.workbuddy.navigation.RouteDefinition

object RouteViewWeek : RouteDefinition{
    override val name = "View Week"
    override val icon = Icons.Rounded.AccountCircle
    override val route = "daytracker/weekview/{week}"
    override val arguments = listOf(
        navArgument(name = "week") {
            type = NavType.StringType
        }
    )
    override val content = { args: Bundle? -> ViewWeekScreen(args) }

    // TODO: Date Validation
    operator fun invoke(navController: NavController, week: String) {
        navController.navigate(route.replace("{week}", week))
    }
}