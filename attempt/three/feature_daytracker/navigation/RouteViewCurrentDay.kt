package com.thedoom92.workbuddy.feature_daytracker.navigation

import android.os.Bundle
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.rounded.AccountCircle
import androidx.navigation.NamedNavArgument
import androidx.navigation.NavController
import com.thedoom92.workbuddy.feature_daytracker.ViewDayScreen
import com.thedoom92.workbuddy.navigation.RouteDefinition

object RouteViewCurrentDay : RouteDefinition{
    override val name = "View Current Day"
    override val icon = Icons.Rounded.AccountCircle
    override val route = "daytracker/dayview"
    override val arguments: List<NamedNavArgument> = emptyList()
    override val content = { _: Bundle? -> ViewDayScreen(null) }

    operator fun invoke(navController: NavController) {
        navController.navigate(route)
    }
}