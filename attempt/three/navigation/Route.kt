package com.thedoom92.workbuddy.navigation

import androidx.compose.runtime.Composable
import androidx.navigation.NavController
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import com.thedoom92.workbuddy.feature_daytracker.navigation.DayTrackerRoutes

object Route {
    val dayTracker = DayTrackerRoutes

    val asList: List<ModuleRoutes> = listOf(
        dayTracker
    )

    @Composable
    fun CreateNavGraph(navController: NavHostController) {
        NavHost(
            navController = navController,
            startDestination = dayTracker.viewCurrentDay.route
        ) {
            // List ALL registered routes
            asList.forEach { it.asList.forEach { rte ->
                composable(
                    route = rte.route,
                    arguments = rte.arguments,
                    deepLinks = emptyList()
                ) { entry ->
                    rte.content(entry.arguments)
                }
            } }
        }
    }
}