package com.thedoom92.workbuddy.navigation

import android.os.Bundle
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.vector.ImageVector
import androidx.navigation.*

interface RouteDefinition {
    val name: String
    val icon: ImageVector
    val route: String
    val arguments: List<NamedNavArgument>
    val content: (arguments: Bundle?) -> @Composable Unit
}