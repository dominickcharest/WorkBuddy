package com.thedoom92.workbuddy.navigation

interface ModuleRoutes {
    val asList: List<RouteDefinition>
}