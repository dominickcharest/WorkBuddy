package com.thedoom92.workbuddy.util

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.receiveAsFlow
import kotlinx.coroutines.launch

open class WBViewModel : ViewModel() {

    private val _stateEvent =  Channel<StateEvent>()
    val stateEvent = _stateEvent.receiveAsFlow()

    private fun sendStateEvent(event: StateEvent) {
        viewModelScope.launch {
            _stateEvent.send(event)
        }
    }
}