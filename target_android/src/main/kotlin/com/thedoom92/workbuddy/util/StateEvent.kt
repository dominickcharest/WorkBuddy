package com.thedoom92.workbuddy.util

sealed class StateEvent {
    object PopBackStack: StateEvent()
    data class Navigate(val route: String): StateEvent()
    data class Snackbar(
        val message: String,
        val action: String? = null
    ): StateEvent()
}