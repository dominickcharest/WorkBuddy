package com.thedoom92.workbuddy

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.rememberNavController
import com.thedoom92.workbuddy.feature_daytracker.dayTrackerNavigation
import com.thedoom92.workbuddy.theme.WorkBuddyTheme
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent {
            WorkBuddyTheme {
                val navController = rememberNavController()
                NavHost(navController = navController, startDestination = Route.Home) {
                    dayTrackerNavigation()
                }
            }
        }
    }
}