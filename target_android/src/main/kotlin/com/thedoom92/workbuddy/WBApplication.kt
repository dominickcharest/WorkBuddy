package com.thedoom92.workbuddy

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class WBApplication : Application ()