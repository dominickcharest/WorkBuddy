package com.thedoom92.workbuddy.feature_daytracker.daydetailscreen

import com.thedoom92.library.time.Moment
import com.thedoom92.workbuddy.feature_daytracker.entity.Shift

data class DDSState(
    val date: String? = null,
    val shifts: MutableList<Shift> = mutableListOf(),

    // Fields
    val shiftStartField: Moment = Moment.fromNow(),
    val shiftEndField: Moment? = null,
    val shiftCustomerField: String = "",
    val shiftTaskField: String = "",

    // Screen state
    val isLoading: Boolean = false,
    val error: String = "",
    val editingShift: Int = -1,
)