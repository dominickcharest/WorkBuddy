package com.thedoom92.workbuddy.feature_daytracker.daydetailscreen.composables

import androidx.compose.material.Text
import androidx.compose.runtime.Composable

@Composable
fun DDSLocationItem(string: String) {
    Text(text = string)
}