package com.thedoom92.workbuddy.feature_daytracker.daydetailscreen.composables

import android.app.TimePickerDialog
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.thedoom92.library.time.Moment

@Composable
fun TimeDisplay(
    value: Moment,
    label: String? = null,
    modifier: Modifier = Modifier,
    showSeconds: Boolean = false,
    onValueChange: (Moment) -> Unit = {}
) {
    val context = LocalContext.current
    Column(
        horizontalAlignment = Alignment.CenterHorizontally,
        modifier = modifier
            .padding(30.dp)
            .clickable {
                TimePickerDialog(
                    context,
                    {_, hour : Int, minute: Int ->
                        val newTime = value.copy(hour = hour, minute = minute)
                        onValueChange(newTime)
                    }, value.hour, value.minute, true
                ).show()
            }
    ) {
        label?.let { Text(label) }

        val format = if (showSeconds) "kk:mm:ss" else "kk:mm"
        Text(text = value.format(format), fontSize = 25.sp)
    }
}