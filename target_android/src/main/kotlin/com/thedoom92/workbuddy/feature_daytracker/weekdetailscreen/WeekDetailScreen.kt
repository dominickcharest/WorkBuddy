package com.thedoom92.workbuddy.feature_daytracker.weekdetailscreen

import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.hilt.navigation.compose.hiltViewModel
import com.thedoom92.workbuddy.util.StateEvent
import kotlinx.coroutines.flow.collect

@Composable
fun WeekDetailScreen (
    viewModel: WeekDetailScreenViewModel = hiltViewModel()
) {
    LaunchedEffect(key1 = true) {
        viewModel.stateEvent.collect { event ->
            when(event) {
                is StateEvent.Snackbar -> {}
                is StateEvent.Navigate -> {}
                is StateEvent.PopBackStack -> {}
            }
        }
    }

}