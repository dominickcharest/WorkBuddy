package com.thedoom92.workbuddy.feature_daytracker

import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavType
import androidx.navigation.compose.composable
import androidx.navigation.navArgument
import com.thedoom92.workbuddy.Route
import com.thedoom92.workbuddy.feature_daytracker.daydetailscreen.DayDetailScreen

fun NavGraphBuilder.dayTrackerNavigation(){
    composable(
        route = Route.DayTracker.DAY_DETAIL,
        arguments = listOf(
            navArgument(name = "date"){
                type = NavType.StringType
                defaultValue = "2021-12-21" //TODO: Today
            }
        )
    ) {
        DayDetailScreen()
    }
}