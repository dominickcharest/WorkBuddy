package com.thedoom92.workbuddy.feature_daytracker.archivescreen

import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.hilt.navigation.compose.hiltViewModel
import com.thedoom92.workbuddy.feature_daytracker.daydetailscreen.DDSViewModel
import com.thedoom92.workbuddy.util.StateEvent
import kotlinx.coroutines.flow.collect

@Composable
fun ArchiveScreen(
    viewModel: DDSViewModel = hiltViewModel()
) {
    LaunchedEffect(key1 = true) {
        viewModel.stateEvent.collect { event ->
            when(event) {
                is StateEvent.Snackbar -> {}
                is StateEvent.Navigate -> {}
                is StateEvent.PopBackStack -> {}
            }
        }
    }
}