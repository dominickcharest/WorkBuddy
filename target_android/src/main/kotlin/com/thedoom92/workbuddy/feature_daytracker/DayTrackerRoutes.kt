package com.thedoom92.workbuddy.feature_daytracker

object DayTrackerRoutes {
    const val DAY_DETAIL = "daytracker/day/{date}"
    const val WEEK_DETAIL = "daytracker/week"
    const val ARCHIVE = "daytracker/archive"
}