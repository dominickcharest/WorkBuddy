package com.thedoom92.workbuddy.feature_daytracker.daydetailscreen

import androidx.compose.runtime.*
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.viewModelScope
import com.thedoom92.library.time.Moment
import com.thedoom92.workbuddy.feature_daytracker.DayTrackerUseCases
import com.thedoom92.workbuddy.feature_daytracker.entity.Day
import com.thedoom92.workbuddy.feature_daytracker.entity.Shift
import com.thedoom92.workbuddy.util.Resource
import com.thedoom92.workbuddy.util.Time
import com.thedoom92.workbuddy.util.WBViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class DDSViewModel @Inject constructor(
    private val useCases: DayTrackerUseCases,
    savedStateHandle: SavedStateHandle
) : WBViewModel() {

    private val _state: MutableState<DDSState> = mutableStateOf(DDSState())
    val state: State<DDSState> = _state

    init {
        val date = savedStateHandle.get<String>("date") ?: Time.today()
        requestDay(date)
    }

    fun onEvent(event: DDSEvent) {
        when (event) {
            is DDSEvent.OnShiftSelect -> {
                // Load the new shift in State
                val shift = _state.value.shifts[event.id]
                _state.value = _state.value.copy(
                    editingShift = event.id,
                    shiftStartField = shift.start_time,
                    shiftEndField = shift.end_time,
                    shiftCustomerField = shift.customer ?: "",
                    shiftTaskField = shift.task ?: ""
                )
            }
            is DDSEvent.OnShiftStartFieldChanged ->
                _state.value = _state.value.copy(shiftStartField = event.start)

            is DDSEvent.OnShiftEndFieldChanged ->
                _state.value = _state.value.copy(shiftEndField = event.end)

            is DDSEvent.OnShiftCustomerFieldChanged ->
                _state.value = _state.value.copy(shiftCustomerField = event.customer)

            is DDSEvent.OnShiftTaskFieldChanged ->
                _state.value = _state.value.copy(shiftTaskField = event.task)


            is DDSEvent.OnStopShiftClicked ->
                _state.value = _state.value.copy(shiftEndField = Moment.fromNow())

            is DDSEvent.OnDismissShiftEditor -> {
                if(event.save) {
                    updateShiftInState()
                    saveDay()
                }

                _state.value = _state.value.copy(editingShift = -1)
            }

            is DDSEvent.OnAddShiftClicked -> {

                // Check if last shift is still active, if it is, trigger OnStopShiftClicked
                val lastShift = _state.value.shifts[_state.value.shifts.size-1]
                lastShift.end_time?.let{
                    onEvent(DDSEvent.OnStopShiftClicked(lastShift))
                }

                // Insert the new shift inside the state
                val shifts = mutableListOf<Shift>()
                shifts.addAll(_state.value.shifts)
                shifts.add(Shift(
                    start_time = Moment.fromNow()
                ))
                _state.value = _state.value.copy(shifts = shifts)

                // Open shift editing dialog
                onEvent(DDSEvent.OnShiftSelect(shifts.size-1))
            }
        }
    }

    private fun requestDay(date: String) {
        useCases.getDay(date).onEach {
            when (it) {
                is Resource.Loading -> {
                    _state.value = DDSState(isLoading = true)
                }
                is Resource.Success -> {
                    val data = it.data ?: return@onEach
                    _state.value = DDSState(
                        date = data.date,
                        shifts = data.shifts,
                    )

                }
                is Resource.Error -> {
                    _state.value = DDSState(error = it.message.toString())
                }
            }
        }.launchIn(viewModelScope)
    }

    private fun updateShiftInState() {
        val index = state.value.editingShift
        state.value.shifts[index] = Shift(
            state.value.shiftStartField,
            state.value.shiftEndField,
            state.value.shiftCustomerField,
            state.value.shiftTaskField,
            null)
    }

    private fun saveDay() {
        viewModelScope.launch {
            useCases.updateDay(Day(
                state.value.date as String,
                state.value.shifts,
                mutableListOf())
            )
        }
    }
}