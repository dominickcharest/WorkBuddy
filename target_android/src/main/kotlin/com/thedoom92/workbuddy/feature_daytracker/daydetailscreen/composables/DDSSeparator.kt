package com.thedoom92.workbuddy.feature_daytracker.daydetailscreen.composables

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.material.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp

@Composable
fun DDSDivider(color: Color = MaterialTheme.colors.primary) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
    ) {
        Box(modifier = Modifier.weight(1f))
        Box(modifier = Modifier.weight(10f).background(color = color).height(1.dp))
        Box(modifier = Modifier.weight(1f))
    }

}