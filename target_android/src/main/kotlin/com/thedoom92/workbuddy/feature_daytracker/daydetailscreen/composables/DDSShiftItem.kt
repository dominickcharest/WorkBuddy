package com.thedoom92.workbuddy.feature_daytracker.daydetailscreen.composables

import android.content.res.Configuration.UI_MODE_NIGHT_YES
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.thedoom92.library.time.Moment
import com.thedoom92.workbuddy.feature_daytracker.daydetailscreen.DDSPreviewMocks
import com.thedoom92.workbuddy.feature_daytracker.entity.Shift
import com.thedoom92.workbuddy.theme.WorkBuddyTheme

@Preview(name = "Regular", uiMode = UI_MODE_NIGHT_YES, showBackground = true)
@Composable
fun DDSClosedShiftItemPreview() {
    WorkBuddyTheme {
        DDSShiftItem(DDSPreviewMocks().getShift())
    }
}

@Composable
fun DDSShiftItem(
    shift: Shift,
    modifier: Modifier = Modifier,
) {
    Surface(
        modifier = modifier
    ) {

        Row(
            modifier = Modifier
                .padding(8.dp)
        ) {
            Column(
                modifier = Modifier.weight(1f)
            ) {
                Text(
                    text = shift.customer ?: "Customer...",
                    style = MaterialTheme.typography.body1,
                    color = MaterialTheme.colors.onBackground
                )
                Text(
                    text = shift.task ?: "Task...",
                    style = MaterialTheme.typography.body2
                )
            }

            val endTime = shift.end_time
            if (endTime == null) {
                ElapsedTimeDisplay(
                    shift.start_time,
                    Modifier.align(Alignment.CenterVertically)
                )
            } else {
                DDSShiftDuration(
                    Moment.Interval(shift.start_time, endTime).print(),
                    Modifier.align(Alignment.CenterVertically)
                )
            }
        }
    }
}