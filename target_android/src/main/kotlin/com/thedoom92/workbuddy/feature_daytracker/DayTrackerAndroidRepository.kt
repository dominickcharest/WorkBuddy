package com.thedoom92.workbuddy.feature_daytracker

import com.thedoom92.workbuddy.feature_daytracker.entity.Day
import com.thedoom92.workbuddy.util.DataNotFoundException

class DayTrackerAndroidRepository(
    private val persistentDAO: DayTrackerDAO
) : DayTrackerRepository {

    override suspend fun getWeek(week: Int): List<Day> {
        return persistentDAO.getWeek(week)
    }

    override suspend fun getWeeks(weeks: IntRange): List<Day> {
        return persistentDAO.getWeeks(weeks)
    }

    override suspend fun initializeCurrentDay(): Day {
        TODO("Not yet implemented")
    }

    override suspend fun getDay(date: String): Day {
        return persistentDAO.getDay(date) ?: throw DataNotFoundException("Couldn't find day $date")
    }

    override suspend fun updateDay(day: Day) {
        return persistentDAO.updateDay(day)
    }
}