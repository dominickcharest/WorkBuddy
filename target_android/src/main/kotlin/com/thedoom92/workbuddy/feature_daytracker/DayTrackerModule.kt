package com.thedoom92.workbuddy.feature_daytracker

import com.thedoom92.workbuddy.feature_daytracker.data.DayTrackerDaoMemory
import com.thedoom92.workbuddy.feature_daytracker.usecases.GetDayUseCase
import com.thedoom92.workbuddy.feature_daytracker.usecases.GetWeekUseCase
import com.thedoom92.workbuddy.feature_daytracker.usecases.UpdateDayUseCase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.scopes.ViewModelScoped

@Module
@InstallIn(ViewModelComponent::class)
object DayTrackerModule {

    @Provides
    @ViewModelScoped
    fun providesUseCase(repository: DayTrackerRepository): DayTrackerUseCases {
        return DayTrackerUseCases(
            UpdateDayUseCase(repository),
            GetDayUseCase(repository),
            GetWeekUseCase(repository))
    }

    @Provides
    @ViewModelScoped
    fun providesRepository(): DayTrackerRepository{
        return DayTrackerAndroidRepository(
            persistentDAO = DayTrackerDaoMemory())
    }
}