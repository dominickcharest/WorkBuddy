package com.thedoom92.workbuddy.feature_daytracker.archivescreen

import com.thedoom92.workbuddy.feature_daytracker.DayTrackerUseCases
import com.thedoom92.workbuddy.util.WBViewModel
import javax.inject.Inject

class ArchiveScreenViewModel @Inject constructor(
    private val action: DayTrackerUseCases

) : WBViewModel() {

}