package com.thedoom92.workbuddy.feature_daytracker.daydetailscreen

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.hilt.navigation.compose.hiltViewModel
import com.thedoom92.workbuddy.feature_daytracker.daydetailscreen.composables.DDSContent


@Composable
fun DayDetailScreen(
    viewModel: DDSViewModel = hiltViewModel()
) {
    val state = viewModel.state.value
    val scaffoldState = rememberScaffoldState()

    Scaffold(
        scaffoldState = scaffoldState,
        floatingActionButton = {
            FloatingActionButton(
                onClick = { viewModel.onEvent(DDSEvent.OnAddShiftClicked)},
                content = { Icon(Icons.Filled.Add, "Add Shift") }
            )
        }
    ) { padding ->
        Box(
            modifier = Modifier.fillMaxSize()
        ) {
            state.error.isNotEmpty().let {
                // Show Error
            }

            if (state.isLoading) {
                CircularProgressIndicator(
                    modifier = Modifier.align(Alignment.Center)
                )
            }

            DDSContent(padding, state, viewModel::onEvent)
        }


    }

    /*
    LaunchedEffect(key1 = true) {
        viewModel.stateEvent.collect { event ->
            when(event) {
                else -> Unit
            }
        }
    }
     */
}