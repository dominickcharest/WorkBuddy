package com.thedoom92.workbuddy.feature_daytracker.daydetailscreen.composables

import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import com.thedoom92.library.time.Moment
import kotlinx.coroutines.delay
import kotlinx.coroutines.isActive

@Composable
fun ElapsedTimeDisplay(start: Moment, modifier: Modifier = Modifier) {
    val currentTime = remember { mutableStateOf(Moment.Interval(start, Moment.fromNow())) }
    LaunchedEffect(true) {
        while (isActive) {
            println("Tick")
            currentTime.value = Moment.Interval(start, Moment.fromNow())
            delay(1000)
        }
    }
    DDSShiftDuration(currentTime.value.print(), modifier)
}