package com.thedoom92.workbuddy.feature_daytracker.daydetailscreen.composables

import android.content.res.Configuration.UI_MODE_NIGHT_YES
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material.Icon
import androidx.compose.material.Snackbar
import androidx.compose.material.Text
import androidx.compose.material.TextField
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Done
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import androidx.compose.ui.window.Dialog
import androidx.compose.ui.window.DialogProperties
import androidx.compose.ui.window.Popup
import androidx.compose.ui.window.SecureFlagPolicy
import com.thedoom92.library.time.Moment
import com.thedoom92.workbuddy.feature_daytracker.daydetailscreen.DDSPreviewMocks
import com.thedoom92.workbuddy.feature_daytracker.daydetailscreen.DDSEvent
import com.thedoom92.workbuddy.feature_daytracker.daydetailscreen.DDSState
import com.thedoom92.workbuddy.theme.WorkBuddyTheme

@Preview(showSystemUi = true, showBackground = true, uiMode = UI_MODE_NIGHT_YES)
@Composable
fun preview() {
    WorkBuddyTheme {
        DDSContent(
            PaddingValues(10.dp),
            DDSPreviewMocks().getState()
        ) {}
    }
}

@Composable
fun DDSContent(paddingValues: PaddingValues, state: DDSState, onEvent: (DDSEvent) -> Unit) {
    Column(
        modifier = Modifier
            .padding(paddingValues)
    ) {
        //DDSSummary(day)
        //DDSActionBar(day)

        LazyColumn {
            item { DDSDivider() }
            items(state.shifts.size) { item ->
                DDSShiftItem(
                    state.shifts[item],
                    Modifier.clickable { onEvent(DDSEvent.OnShiftSelect(item)) },
                )
                DDSDivider()
            }
        }

        // Shift Editor Popup
        if(state.editingShift >= 0) {
            Dialog(
                onDismissRequest = {},
                properties = DialogProperties(
                    dismissOnBackPress = false,
                    dismissOnClickOutside = false,
                    securePolicy = SecureFlagPolicy.SecureOff
                ),
                content = { DDSShiftEditor(state, onEvent) }
            )
        }

        /*LazyColumn {
            items(state.locations.size) { item ->
                DDSLocationItem(state.locations[item])
            }
        }*/
    }
}