package com.thedoom92.workbuddy.feature_daytracker.daydetailscreen.composables

import androidx.compose.foundation.layout.padding
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp

@Composable
fun DDSShiftDuration(value: String, modifier: Modifier = Modifier) {
    Text(
        text = value,
        style = MaterialTheme.typography.h5,
        modifier = modifier
            .padding(4.dp)
    )
}