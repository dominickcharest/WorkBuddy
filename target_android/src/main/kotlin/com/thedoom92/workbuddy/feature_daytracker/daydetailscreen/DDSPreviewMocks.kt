package com.thedoom92.workbuddy.feature_daytracker.daydetailscreen

import com.thedoom92.workbuddy.feature_daytracker.data.DayTrackerDaoMemory
import com.thedoom92.workbuddy.feature_daytracker.entity.Day
import com.thedoom92.workbuddy.feature_daytracker.entity.Shift
import kotlinx.coroutines.runBlocking

class DDSPreviewMocks {

    private val day = runBlocking { DayTrackerDaoMemory().getDay("2021-12-21") }!!

    fun getEditShiftState(): DDSState {
        return DDSState(
            date = day.date,
            shifts = day.shifts,
            shiftCustomerField = day.shifts[0].customer ?: "",
            shiftTaskField = day.shifts[0].task ?: "",
            editingShift = 0
        )
    }

    fun getState(): DDSState {
        return DDSState(
            date = day.date,
            shifts = day.shifts
        )
    }

    fun getDay(): Day {
        return day
    }

    fun getShift(): Shift {
        return day.shifts[0]
    }
}