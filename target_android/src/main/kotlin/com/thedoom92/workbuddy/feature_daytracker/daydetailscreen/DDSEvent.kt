package com.thedoom92.workbuddy.feature_daytracker.daydetailscreen

import com.thedoom92.library.time.Moment
import com.thedoom92.workbuddy.feature_daytracker.entity.Shift

sealed class DDSEvent {


    data class OnShiftSelect(val id: Int) : DDSEvent()
    data class OnStopShiftClicked(val shift: Shift) : DDSEvent()
    data class OnDismissShiftEditor(val save: Boolean) : DDSEvent()
    object OnAddShiftClicked : DDSEvent()

    data class OnShiftStartFieldChanged(val start: Moment) : DDSEvent()
    data class OnShiftEndFieldChanged(val end: Moment) : DDSEvent()
    data class OnShiftCustomerFieldChanged(val customer: String) : DDSEvent()
    data class OnShiftTaskFieldChanged(val task: String) : DDSEvent()

}