package com.thedoom92.workbuddy.feature_daytracker.daydetailscreen.composables

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Done
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import com.thedoom92.library.time.Moment
import com.thedoom92.workbuddy.feature_daytracker.daydetailscreen.DDSEvent
import com.thedoom92.workbuddy.feature_daytracker.daydetailscreen.DDSState

@Composable
fun DDSShiftEditor(
    state: DDSState, onEvent: (DDSEvent) -> Unit = {}
) {
    Column(
        modifier = Modifier.fillMaxWidth().border(width = 1.dp, color = MaterialTheme.colors.primary)
            .background(color = MaterialTheme.colors.background)
    ) {
        Row(
            horizontalArrangement = Arrangement.SpaceEvenly, modifier = Modifier.fillMaxWidth()
        ) {

            // Two time display at the top
            TimeDisplay(
                value = state.shiftStartField ?: Moment.fromNow(),
                label = "Start Time",
                onValueChange = {
                    onEvent(DDSEvent.OnShiftStartFieldChanged(it))
                },
            )
            if (state.shiftEndField != null) {
                TimeDisplay(
                    value = state.shiftEndField,
                    label = "End Time",
                    onValueChange = {
                        onEvent(DDSEvent.OnShiftEndFieldChanged(it))
                    },
                )
            } else {
                Column(horizontalAlignment = Alignment.CenterHorizontally,
                    modifier = Modifier
                        .padding(30.dp)
                        .clickable { onEvent(DDSEvent.OnStopShiftClicked(
                                state.shifts[state.editingShift]
                        ))}
                ) {
                    Text("End Shift")
                    Icon(Icons.Default.Done, "Stop Shift")
                }
            }
        }

        // Fields
        TextField(
            value = state.shiftCustomerField,
            onValueChange = { onEvent(DDSEvent.OnShiftCustomerFieldChanged(it)) },
            placeholder = { Text("Customer") },
            modifier = Modifier.fillMaxWidth()
        )
        TextField(
            value = state.shiftTaskField,
            onValueChange = { onEvent(DDSEvent.OnShiftTaskFieldChanged(it)) },
            placeholder = { Text("Task") },
            modifier = Modifier.fillMaxWidth()
        )

        // Buttons
        Row(
            modifier = Modifier.fillMaxWidth(),
            horizontalArrangement = Arrangement.SpaceEvenly
        ) {
            TextButton(
                onClick = { onEvent(DDSEvent.OnDismissShiftEditor(false)) }
            ) {
                Text("Discard")
            }

            TextButton(
                onClick = { onEvent(DDSEvent.OnDismissShiftEditor(true)) }
            ) {
                Text("Save")
            }
        }
    }
}