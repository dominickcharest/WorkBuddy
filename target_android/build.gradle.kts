val ktKotlinVer: String by project
val ktSerializationVer: String by project

val axActivityVer: String by project
val axAppCompatVer: String by project
val axComposeVer: String by project
val axComposeCompilerVer: String by project
val axLifecycleVer: String by project
val axNavigationVer: String by project
val axWorkVer: String by project
val axGlanceVer: String by project
val axHiltVer: String by project
val hiltVer: String by project

plugins {
    id("com.android.application")
    id("kotlin-android")
    kotlin("plugin.serialization") version "1.6.10"
    kotlin("kapt")
    id("dagger.hilt.android.plugin")
    id("androidx.navigation.safeargs.kotlin")
}

repositories {
    mavenCentral()
    google()
    maven("https://androidx.dev/snapshots/latest/artifacts/repository")
}

dependencies {
    implementation(project(":target_shared"))
    implementation(project(":library"))

    // Kotlinx Serialisation
    // implementation("org.jetbrains.kotlin:kotlin-serialization:$ktKotlinVer")
    implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:$ktSerializationVer")

    // Android
    implementation("com.google.android.material:material:1.4.0")

    // Android KTX
    implementation("androidx.core:core-ktx:1.7.0")
    implementation("androidx.collection:collection-ktx:1.2.0")
    //implementation("androidx.fragment:fragment-ktx:1.4.0")
    implementation("androidx.lifecycle:lifecycle-runtime-ktx:$axLifecycleVer")
    implementation("androidx.navigation:navigation-runtime-ktx:$axNavigationVer")
    implementation("androidx.navigation:navigation-fragment-ktx:$axNavigationVer")
    implementation("androidx.navigation:navigation-ui-ktx:$axNavigationVer")
    //implementation("androidx.palette:palette-ktx:1.0.0")
    implementation("androidx.lifecycle:lifecycle-viewmodel-ktx:$axLifecycleVer")
    implementation("androidx.work:work-runtime-ktx:$axWorkVer")

    // Jetpack Activity
    //implementation("androidx.activity:activity-ktx:$axActivityVer ")
    implementation("androidx.activity:activity-compose:$axActivityVer")

    // Jetpack AppCompat
    implementation("androidx.appcompat:appcompat:$axAppCompatVer")
    //implementation("androidx.appcompat:appcompat-resources:$axAppCompatVer")

    // Jetpack Compose
    implementation("androidx.compose.animation:animation:$axComposeVer")
    implementation("androidx.compose.compiler:compiler:$axComposeCompilerVer")
    implementation("androidx.compose.foundation:foundation:$axComposeVer")
    implementation("androidx.compose.material:material:$axComposeVer")
    //implementation("androidx.compose.material3:material3:1.0.0-alpha02")
    implementation("androidx.compose.runtime:runtime:$axComposeVer")
    implementation("androidx.compose.ui:ui:$axComposeVer")
    implementation("androidx.compose.ui:ui-tooling:$axComposeVer")
    implementation("androidx.compose.ui:ui-tooling-preview:$axComposeVer")

    // Jetpack Lifecycle
    kapt("androidx.lifecycle:lifecycle-compiler:$axLifecycleVer")
    implementation("androidx.lifecycle:lifecycle-viewmodel-compose:$axLifecycleVer")
    implementation("androidx.lifecycle:lifecycle-viewmodel-savedstate:$axLifecycleVer")

    // Jetpack Navigation
    implementation("androidx.navigation:navigation-dynamic-features-fragment:$axNavigationVer")
    implementation("androidx.navigation:navigation-compose:$axNavigationVer")

    // Jetpack Work
    implementation("androidx.work:work-multiprocess:$axWorkVer")

    // Jetpack Glance
    //implementation("androidx.glance:glance:$axGlanceVer")
    //implementation("androidx.glance:glance-appwidget:$axGlanceVer")

    // Jetpack Hilt
    kapt("com.google.dagger:hilt-android-compiler:$hiltVer")
    kapt("androidx.hilt:hilt-compiler:1.0.0")
    implementation("com.google.dagger:hilt-android:$hiltVer")
    implementation("androidx.hilt:hilt-lifecycle-viewmodel:1.0.0-alpha03")
    implementation("androidx.hilt:hilt-navigation-compose:1.0.0-rc01")

    // Google Maps SDK
    // implementation("com.google.maps.android:maps-ktx:$gmapVer")
    // implementation("com.google.maps.android:maps-utils-ktx:$gmapVer")
}

android {
    compileSdk = 31

    defaultConfig {
        applicationId = "com.thedoom92.workbuddy"
        minSdk = 27
        targetSdk = 32
        versionCode = 1
        versionName = rootProject.version.toString()

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
        vectorDrawables {
            useSupportLibrary = true
        }
    }

    buildTypes {
        release {
            isMinifyEnabled = false
            proguardFiles(getDefaultProguardFile("proguard-android-optimize.txt"), "proguard-rules.pro")
        }
    }
    compileOptions {
        sourceCompatibility(JavaVersion.VERSION_1_8)
        targetCompatibility(JavaVersion.VERSION_1_8)
    }
    kotlinOptions {
        jvmTarget = "1.8"
    }
    buildFeatures {
        compose = true
    }
    composeOptions {
        kotlinCompilerExtensionVersion = axComposeCompilerVer
    }
    packagingOptions {
        resources {
            excludes += "/META-INF/{AL2.0,LGPL2.1}"
        }
    }
}

// Allow references to generated code
kapt {
    correctErrorTypes = true
}