import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    id("pl.allegro.tech.build.axion-release") version "1.13.6"
}

buildscript {

    repositories {
        mavenLocal()
        mavenCentral()
        google()
    }
    dependencies {
        val ktKotlinVer: String by project
        val gradleVer: String by project
        val hiltVer: String by project
        val axNavigationVer: String by project

        classpath("com.android.tools.build:gradle:$gradleVer")
        classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:$ktKotlinVer")
        classpath("com.google.dagger:hilt-android-gradle-plugin:$hiltVer")
        classpath("androidx.navigation:navigation-safe-args-gradle-plugin:$axNavigationVer")
    }
}

group = "com.thedoom92"
version = scmVersion.version

allprojects {
    tasks.withType<KotlinCompile>().configureEach {
        kotlinOptions.jvmTarget = "1.8"
    }
}

task("clean") {
    delete(rootProject.buildDir)
}